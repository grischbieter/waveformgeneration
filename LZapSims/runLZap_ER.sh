#!/bin/bash
#source setup.sh
source /cvmfs/lz.opensciencegrid.org/LZap/latest/x86_64-centos7-gcc7-opt/setup.sh

ls -1 ../DERsims/ERfiles/lz_${1}*.root > input_${1}.txt

export LZAP_INPUT_FILES=input_${1}.txt
export LZAP_OUTPUT_FILE="ER_output_${1}.root"

lzap RunLZapMDC3PodWaveforms.py 
