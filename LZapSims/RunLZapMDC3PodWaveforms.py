from Gaudi.Configuration import *
import os
from MDC3.common_io import *
from MDC3.common_lzap_modules import *

inputFiles = getInputFiles()

sparseInputFile = os.getenv('LZAP_SPARSE_INPUT')

try:
    outputFile = os.environ['LZAP_OUTPUT_FILE']
except KeyError:
    outputFile = "lzap_waveforms.root"

mcOutputFile = outputFile.replace('.root', '_mctruth.root')

app = ApplicationMgr()
app.EvtMax = -1
app.HistogramPersistency = "NONE"
app.EvtSel = "Ldrf"
app.OutputLevel = 3



#################################################
# Configure inputs, outputs
selector    = configure_LDRF_inputs(app, inputFiles)
if sparseInputFile is not None:
    # if selector.SparseEventFile is a non-empty string, LZap ignores all
    # other inputs and runs in sparse processing mode
    selector.SparseEventsFile = sparseInputFile
condSvc     = configure_conditions_svc(app)

from RQModules import RQModulesConf
rqConversionSvc = RQModulesConf.RQ__RQConversionSvc('RQConversionSvc')
rqConversionSvc.SaveRQMCTruth = True
rqConversionSvc.SavePodWaveforms = True
addresses = pdm_to_rq_addresses()
addresses += [
    '/Event/TpcHighGain/SummedPods',
    '/Event/TpcLowGain/SummedPods',
    '/Event/Skin/SummedPods',
    '/Event/OuterHighGain/SummedPods',
    '/Event/OuterLowGain/SummedPods',
]
outStream   = configure_RQ_output(app, outputFile, addresses)
mcOutStream = configure_RQMCTruth_output(app, mcOutputFile)
app.OutStream += [ outStream, mcOutStream ]


#################################################
# Define processing chain
# Order is important. 
modules = OrderedDict()
modules.update(common_modules())
modules.update(tpc_modules())
modules.update(skin_modules())
modules.update(od_modules())
modules.update(interaction_modules())
modules.update(dqm_modules())

# module parameter customizations
set_standard_module_parameters(modules)

app.TopAlg = prepare_chain_for_lzap(modules)
